# -*- coding: utf-8 -*-
"""
Created on Tue Sep 10 19:15:17 2019

@author: MSIvino
"""

"""
Divine Maloney
September 16, 2019
CPSC 6820 : Intro to Machine Learning
Dr. Hodges
P1

This program does the following steps:
-. Randomize the fish data
-. Divide the file into two files, A test set with 60 records and a training set with 240
-. Divide in the training set into 5 subsets(folds)
-. Group the folds into 5 training/validation set groups
-. Write a Python KNN program and test each of the 5 groups with multiple values of k
-. Pick the k with the best overall performance
-. Use that k in a python KNN program with entire 240 record training set and test set
-  Analyze, create figures, and write up your results
-  Create python program to turn in 
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import StandardScaler

#Input: Read text file named "FF43.txt" and add in headers to the data
rawFishData = pd.read_csv("threeToxinsNumbered.txt", 
                      delimiter = '\t',
                      names=['BodyLength','FinLength','FishType']
                       )
#mark FishData as Dataframe
FishData = pd.DataFrame(rawFishData)
#Get labled fish data and then count how many rows 
num_lines = sum(1 for line in open('threeToxinsNumbered.txt'))
NumLabelData = num_lines - 1 

#How man count labelled data points and subtract it by 1, remove the row
FishData = FishData.drop([0])
FishData.head()
#Randomize the data and Columsn and Rows
#FishData = FishData.apply(np.random.permutation, axis=1)

#FishData randomize columns
FishData = FishData.sample(frac=1, random_state=200)

#create samples of the randomize data 20% Test set & 80% Training
FishData_Test = FishData.sample(frac=.2)
FishData_Train = FishData.sample(frac=.8)

#Divide in the training set into 5 subsets/folds
FishData_TrainSet_1 = FishData_Train.sample(frac=.2)
FishData_TrainSet_2 = FishData_Train.sample(frac=.2)
FishData_TrainSet_3 = FishData_Train.sample(frac=.2)
FishData_TrainSet_4 = FishData_Train.sample(frac=.2)
FishData_TrainSet_5 = FishData_Train.sample(frac=.2)

#Group subsets into 5 training/validation set groups

FishData_TrainSet_1_Val = FishData_TrainSet_1
FishData_TrainSet_1_Tr = FishData_TrainSet_1


#print((FishData_TrainSet_1))
#print((FishData_TrainSet_2))
#print((FishData_TrainSet_3))
#print((FishData_TrainSet_4))
#print((FishData_TrainSet_5))

scaler = StandardScaler()
scaler.fit(FishData_Train)


print((FishData_Train))

FishData_Train = scaler.transform(FishData_Train)
#X_test = scaler.transform(X_test)


#print(FishData_Test_count)

#print("total Count of Data"  + NumLabelData)


#https://github.com/Madhu009/Deep-math-machine-learning.ai/blob/master/Knn_Scratch.ipynb
#https://medium.com/@kbrook10/day-11-machine-learning-using-knn-k-nearest-neighbors-with-scikit-learn-350c3a1402e6
#https://machinelearningmastery.com/tutorial-to-implement-k-nearest-neighbors-in-python-from-scratch/
#https://www.edureka.co/blog/k-nearest-neighbors-algorithm/ ACTUAL ALGORITHM
#https://towardsdatascience.com/train-test-split-and-cross-validation-in-python-80b61beca4b6
#https://stackabuse.com/k-nearest-neighbors-algorithm-in-python-and-scikit-learn/
#https://stackoverflow.com/questions/24147278/how-do-i-create-test-and-train-samples-from-one-dataframe-with-pandas?noredirect=1&lq=1
#https://chrisalbon.com/machine_learning/trees_and_forests/random_forest_classifier_example/