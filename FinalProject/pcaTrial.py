# -*- coding: utf-8 -*-
"""
Created on Sun Nov 17 17:23:29 2019

@author: divin
"""

import matplotlib.pyplot as plt
import plotly.plotly as py

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score


df = pd.read_csv("threeToxinsNumbered.txt", 
                      delimiter = '\t',
                      names=['Air','Land','Water','State']
                       )

df.columns= ['Air', 'Land', 'Water','State']


X = df.iloc[:,0:3].values
y = df.iloc[:,3].values


# plotting histograms
data = []

legend = {0:False, 1:False, 2:True}

colors = {'Air': '#0D76BF', 
          'Land': '#00cc96', 
          'Water': '#EF553B'}

for col in range(3):
    for key in colors:
        trace = dict(
            type='histogram',
            x=list(X[y==key, col]),
            opacity=0.75,
            xaxis='x%s' %(col+1),
            marker=dict(color=colors[key]),
            name=key,
            showlegend=legend[col]
        )
        data.append(trace)

layout = dict(
    barmode='overlay',
    xaxis=dict(domain=[0, 0.25], title='Air'),
    xaxis2=dict(domain=[0.3, 0.5], title='Land'),
    xaxis3=dict(domain=[0.55, 0.75], title='Water'),
    yaxis=dict(title='count'),
    title='Distribution of the different toxic release features'
)

fig = dict(data=data, layout=layout)
py.iplot(fig, filename='exploratory-vis-histogram')