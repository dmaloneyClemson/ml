# Project 3 Assignment
# For Intro to ML
# Alex Adkins
# Oct 18, 2019


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

class Dataset():
	def __init__(self, fileName, col_names):
		# self.randomized = False
		self.normalized = False
		self.col_names = col_names

		self.data = self.import_data_from_csv(fileName, col_names)
		self.orig_data = self.data.copy()


	# Check if data file exists
	def check_file_exists(self, file_name, col_names):
		try:
			# Try opening the file to check if file valid, close file as to not leaving hanging data
			# os.path.isfile is a faster/more efficient check, but may lead to other issues, does not return r/w permissions
			# open() is a decent check here for a singular data file
			file = open(file_name, "r")
			file.close()
		except FileNotFoundError:
			file_name = input(file_name + " file not found. \nPlease try another file name (try adding the path): ")
			self.import_data_from_csv(file_name, col_names)
		except:
			print("Unknown error when retreiving data file.")

	# Imports the data into a pandas dataframe
	def import_data_from_csv(self, file_name, col_names):
		self.check_file_exists(file_name, col_names)

		panda_data = pd.read_csv(file_name, skiprows=[0], delimiter="\t",
								names=col_names)

		return (panda_data)

	# # Randomizes and returns the imported data
	# def randomize_data(self):
	# 	# Drops record of previous data positions
	# 	self.data = self.data.sample(frac=1).reset_index(drop=True)
	# 	self.randomized = True
	# 	# return(data)

	# Normalize the data using standard deviation normalization
	# If no columns specified, normalizes all data
	def normalize_data(self, normalize_columns=None):
		self.orig_mean = self.data.mean()
		self.orig_std = self.data.std()

		normalize_columns=["state"]

		if normalize_columns == None:
			self.data=(self.data-self.orig_mean)/self.orig_std
		else:
			for col_name, col in self.data.iteritems():
				if col_name not in normalize_columns:
					self.data[col_name] = (self.data[col_name]-self.data[col_name].mean())/self.data[col_name].std()
					
		self.normalized = True

	# TODO: fix when re-doing this in polynomial and alpha setting

	def create_training_and_test_sets(self):
		# if self.normalized != True:
		# 	self.normalize_data()
		msk = np.random.rand(len(self.data)) < 0.7
		self.training = self.data[msk]
		self.testing = self.data[~msk]

class Hypothesis():
	def __init__(self, df):
		self.polynomial = 3
		self.alpha = .2
		self.iterations = 100

		self.df = df
		
		self.ws = []
		self.instantiate_ws()
		self.instantiate_xs()

		self.calculated_ws = False
		self.save_costs = False
		self.costs = []

	def instantiate_ws(self): 
		self.ws = []

		ws_start = 0
		self.ws.append(ws_start)

		for p in range(1,self.polynomial+1):
			for j in range(1,4):
				self.ws.append(ws_start)

	def instantiate_xs(self):
		x1 = self.df.data[self.df.col_names[0]]
		x2 = self.df.data[self.df.col_names[1]]
		# self.df.data["x1"] = self.df.data[self.df.col_names[0]]
		self.df.data["x1"] = x1
		self.df.data["x2"] = x2
		for i in range(1,self.polynomial+1):
			for j in range(1,4):
				x_n = (i-1) * 3 + j
				data_str = "x" + str(x_n)
				# print("data_str: ", data_str)
				if j%3 == 1:
					self.df.data[data_str] = x1**i
				if j%3 == 2:
					self.df.data[data_str] = x2**i
				if j%3 == 0:
					self.df.data[data_str] = (x1**i) * (x2**i)
				# x_n = x_n + 1
		# print("what: ", self.df.data.head())
		self.df.normalize_data()
		self.df.create_training_and_test_sets()
		

	def set_alpha(self, alpha):
		self.alpha = alpha
		self.instantiate_ws()
		self.instantiate_xs()
		self.calculated_ws = False

	def set_polynomial(self, polynomial):
		self.polynomial = polynomial
		self.instantiate_ws()
		self.instantiate_xs()
		self.calculated_ws = False

	def set_save_costs(self, save):
		if save == True:
			self.save_costs = True
		else:
			self.save_costs = False

	def set_iterations(self, iterations):
		self.iterations = iterations

	def get_J(self):
		if self.costs != []:
			return self.costs[-1]
		else:
			print("Costs have not been saved, cannot return J.")
			print_section_separator()

	def hypo_func(self, row):
		ans = 0
		# calculate the answer up to however many polynomials you want (x^polynomial)
		# return self.ws[0] + (self.ws[1] * row[self.df.col_names[0]]) + (self.ws[2] * row[self.df.col_names[1]])

		for i in range(self.polynomial+1):
			if i == 0:
				ans = ans + self.ws[0]
				continue

			for j in range(1,4):
				x_n = (i-1) * 3 + j

				# ans = ans + self.ws[x_n] * self.df.data[x_n]
				x_str = "x" + str(x_n)

				ans = ans + (self.ws[x_n] * row[x_str])
				# if j%3 == 1: 
				# 	ans = ans + self.ws[i] * row[0]**i
				# 	continue

				# if j%3 == 2: 
				# 	ans = ans + self.ws[i] * row[1]**i
				# 	continue

				# if j%3 == 0:
				# 	ans = ans + self.ws[i] * row[0]**i * row[1]**i
				# 	# exp = exp+1
				# 	continue
		if not isinstance(ans, float):
			return ans.get(0)
		return ans

	def hypo_func_log(self, row):
		return 1/(1 + np.exp(-1*self.hypo_func(row)))

	def predict_output(self, row): 
		# TODO: check return values are good

		y = self.hypo_func_log(row)
		# print("estimate output y: ", y)
		if y >= 0.5:
			return 1
		else:
			return 0

	def cost_func(self, row, y):
		return ((-1 * y) * (np.log(self.hypo_func_log(row)))) - ((1 - y) * (np.log(1 - self.hypo_func_log(row))))

	def summed_cost(self):
		m = len(self.df.training)
		
		cost_func_sum = 0

		for i, row in self.df.training.iterrows():
			y = row[self.df.col_names[2]]

			cost_func_sum = cost_func_sum + self.cost_func(row, y)

		return (1/m * cost_func_sum)

	# Determine ws thru gradient descent
	def update_ws_once(self):
		tempws = [None] * len(self.ws)
		for w in range(len(self.ws)):
			val = 0
			for i, row in self.df.training.iterrows():
				y = row[self.df.col_names[2]]

				x_str = "x" + str(w)

				if w == 0:
					val = val + (self.hypo_func_log(row) - y)
				else:
					val = val + (self.hypo_func_log(row) - y) * row[x_str]

			tempws[w] = self.ws[w] - 1/len(self.df.training) * self.alpha * val

		if self.save_costs:
			cost = self.summed_cost()
			self.costs.append(cost)

		self.ws = tempws

	def update_ws(self, iterations = 100):
		if iterations != 100:
			self.set_iterations(iterations)

		for i in range(self.iterations):
			self.update_ws_once()

		self.calculated_ws = True

	def pos_neg(self, positive, negative, correct, predicted):
		if correct == positive and predicted == positive:
			self.true_pos = self.true_pos + 1
		elif correct == negative and predicted == negative:
			self.true_neg = self.true_neg + 1
		elif correct == negative and predicted == positive:
			self.false_pos = self.false_pos + 1
		elif correct == positive and predicted == negative:
			self.false_neg = self.false_neg + 1

	def test_hypo(self):
		self.true_pos = 0
		self.false_pos = 0
		self.true_neg = 0
		self.false_neg = 0

		positive = 1
		negative = 0

		for i, row in self.df.testing.iterrows():
			correct_ans = row[2]

			predicted_ans = self.predict_output(row)

			self.pos_neg(positive, negative, correct_ans, predicted_ans)

		print("\n")
		print("True Positive: ", self.true_pos)
		print("True Negative: ", self.true_neg)
		print("False Positive: ", self.false_pos)
		print("False Negative: ", self.false_neg)

		accuracy = (self.true_pos + self.true_neg)/(self.true_pos + self.true_neg + self.false_pos + self.false_neg)
		precision = self.true_pos / (self.true_pos + self.false_pos)
		recall = self.true_pos / (self.true_pos + self.false_neg)
		f1 = 2 * (1/((1/precision) + (1/recall)))

		print("\n")
		print("Accuracy: ", round(accuracy, 2))
		print("Precision: ", round(precision, 2))
		print("Recall: ", round(recall, 2))
		print("F1: ", round(f1, 2))

		print("Final ws: ")
		for w in range(len(self.ws)):
			# print("\tw"+str(w)+": ", round(self.ws[w], 2))
			print("\tw"+str(w)+": ", self.ws[w])

		if self.save_costs:
			print("\n")
			print("Final J: ", self.costs[-1])





class Plots():
	def __init__(self, df, hypo):
		self.df = df
		self.hypo = hypo

	# Plot the costs over iterations
	# Takes user input
	def plot_costs(self, min_i=0, max_i=None):
		if self.hypo.costs == []:
			hypo.set_save_costs(True)
			hypo.update_ws()
			print("Thank you for your patience as costs are calculated.")
			print_section_separator()

		if max_i == None:
			max_i = len(self.hypo.costs)+1

		if max_i > len(self.hypo.costs):
			max_i = len(self.hypo.costs)

		iterations = []
		for i in range(min_i, max_i):
			iterations.append(i)

		plt.scatter(iterations, self.hypo.costs[min_i:max_i])
		plt.xlabel("Iteration")
		plt.ylabel("Cost (J)")
		plt.title("Cost (J) Over Iterations")
		plt.savefig("adkins_J.png")
		plt.show()

	def raw_data_plt(self):
		fish0 = self.df.orig_data.loc[self.df.orig_data["state"]==1]
		fish1 = self.df.orig_data.loc[self.df.orig_data["state"]==2]
		plt.scatter(fish0["air"], fish0["water"], color="cyan", marker=".", label="TigerFish0")
		plt.scatter(fish1["air"], fish1["water"], color="orange", marker=".", label="TigerFish1")

		return plt

	def plot_raw_data(self):
		plt = self.raw_data_plt()

		# Plot meta data
		plt.xlabel("Body Length")
		plt.ylabel("Fin Length")
		plt.title("Body Length vs. Fin Length for types of TigerFish")
		plt.legend(loc="lower right")
		plt.savefig("rawdata.jpg")
		plt.show()

	# def plot_decision_boundary(self):
	# 	plt = self.raw_data_plt()

	# 	plt.show()

def get_file_name(file_name):
	print_section_separator()
	print("Provide data file name.")
	print("Press enter to use default: FF01.txt")
	new_file_name = str(input("File name: "))
	if new_file_name != "":
		file_name = new_file_name
	return file_name

# For handy user interface
def print_section_separator():
	print("\n-------------\n")

def user_testing(hypo):
	print_section_separator()
	print("Please test out this regression machine learning algorithm.")
	print("You will be prompted to enter a fish's body length, and fin length. \n\tThe algorithm will predict what kind of fish it is.")
	print("To exit the program, enter 0 for each value.")
	get_user_predicted_y_with_x_inputs(hypo)

def normalize_x(x, data_mean, data_std):
	# print("\n normalize: ")
	# print("x: ", x)
	# print("data_mean: ", data_mean)
	# print("data_std: ", data_std)
	xN = (x-data_mean)/data_std
	# print("xN: ", xN)
	return xN

def normalize_row(row, hypo):
	means = hypo.df.orig_mean
	stds = hypo.df.orig_std
	# print("\n\n")
	# print(row)

	new_xs = []
	new_cols = []

	for col_name, col in row.iteritems():	
		# print("colname: ",col_name)
		if col_name == 0:
			continue
		# if isinstance(col_name, str):
		# 	continue

		if col_name in ["air", "water", "state"]:
			continue
		x = row.iloc[0][col_name]
		# print(x)
		# print("means: ", means)
		# print("means col: ", means[col_name])
		x = normalize_x(x, means[col_name], stds[col_name])
		new_xs.append(x)
		new_cols.append(col_name)
		# row.iloc[0][col_name] = x
		# row[0][col_name] = x
	
	# print(new_xs)
	# print(new_cols)
	# row = pd.DataFrame(new_xs)
	newrow = pd.DataFrame([new_xs], columns = new_cols)

	# testrow=(row-means)/stds

	# print("newrow: ", newrow)
	# print("testrow: ", testrow)

	return newrow
	# print("row", row)




def get_xs(x1, x2, polynomial):
	# x1 = self.df.data[self.df.col_names[0]]
	# x2 = self.df.data[self.df.col_names[1]]
	# self.df.data["x1"] = self.df.data[self.df.col_names[0]]
	# self.df.data["x1"] = x1
	# self.df.data["x2"] = x2

	fish_data = ["air", "water", "state"]
	# fish_data = ["FISH", ""]
	df = pd.DataFrame([fish_data], columns=["air", "water", "state"])
	# df = pd.DataFrame(fish_data)
	for i in range(1,polynomial+1):
		for j in range(1,4):
			x_n = (i-1) * 3 + j
			data_str = "x" + str(x_n)
			# print("data_str: ", data_str)
			if j%3 == 1:
				df[data_str] = x1**i
			if j%3 == 2:
				df[data_str] = x2**i
			if j%3 == 0:
				df[data_str] = (x1**i) * (x2**i)
	# print(df)
	return df

def get_user_predicted_y_with_x_inputs(hypo):
	print("\n")
	print("Enter the fish's values.")

	escape = False
	try:
		air = float(input("Body Length: "))
		water = float(input("Fin Length: "))
	except ValueError:
		print("Please enter a valid positive int or float.")
		escape = get_user_predicted_y_with_x_inputs(hypo)
	except:
		print("Unknown error. Please try again.")
		escape = get_user_predicted_y_with_x_inputs(hypo)

	# print("body len: ", air)
	# print("fin len: ", water)
	if (air == 0 and water == 0):
		return (True)

	if (air < 0 or water < 0) or (air < 0 or water < 0):
		print("Please enter valid positive integers.")
		escape = get_user_predicted_y_with_x_inputs(hypo)

	if escape == True:
		return escape

	# air = normalize_x(air, data_means["studying_min"], data_stds["studying_min"])

	fish_row = get_xs(air, water, hypo.polynomial)

	fish_row = normalize_row(fish_row, hypo)

	# print("fishrow: ", fish_row)

	# air = normalize_x(air, hypo.df.orig_mean, hypo.df.orig_std)
	# water = normalize_x(water, hypo.df.orig_mean, hypo.df.orig_std)
	y = hypo.predict_output(fish_row)
	# print(y)
	# water = normalize_x(water, data_means["beer_oz"], data_stds["beer_oz"])
	# print("study and beer normalize: ", air, water)
	# y = hypothesis_func(air, water, ws)
	print("The fish's predicted type is: ", y)
	escape = get_user_predicted_y_with_x_inputs(hypo)

def main():
	fileName = "threeToxinsNumberedFinal.txt"
	col_names = ["air", "water", "state"]

	fileName = get_file_name(fileName)

	df = Dataset(fileName, col_names)

	hypo = Hypothesis(df)
	# hypo.set_polynomial(3)
	# hypo.set_alpha(.2)
	# hypo.set_save_costs(True)
	hypo.update_ws(iterations = 150)

	hypo.test_hypo()

	user_testing(hypo)



	# plots = Plots(df, hypo)
	# plots.plot_costs()
	# plots.plot_raw_data()
	# plots.plot_decision_boundary()




main()







