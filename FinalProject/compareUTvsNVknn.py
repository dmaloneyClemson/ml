# Alex CompareState
# September 6, 2019
# CPSC 6820 : Intro to Machine Learning
# Dr. Hodges

# use a test set and 5-fold cross validation to determine the best value of k (best number of neighbors in preduction)
# write up shuold be less than 2 pages
# Should include:
# problem description
# description of data set along with plot of the data
# description of procedure
# figure showing how many misidentifications you had for each training/validation set combo for diff values of k
# a plot of average accuracy for diff values of k
# confusion matrix shoign your results on the final test set
# description of your final results that includes choice of k, accuracy, precision, recall, and f1

# upload: 
# complete write up as pdf file (CompareState_Alex_P1Report.pdf)
# single python file (CompareState_Alex_P1.py)
# 	that prompts for the name of a file containing labelled training data
# 	then repeatedly prompts for the body length and dorsal fin length

# Rec steps:
# 1. randomize fish data
# 2. divide file into test set (60 records) and training set (240)
# 3. divide training set into 5 subsets (folds)
# 4. group the folds into 5 training/validation set groups
# 5. write a python kNN program and test each of the 5 groups with multiple values of k
# 6. pick the k with the best overall performance
# 7. use that k in a python kNN program with the entire 240 record training set and the test set
# 8. analyze, create figures, and write up your results
# 9. create python program to turn in
# 10. upload written report and python program

# The file: 
# 1st line: integer indiciating how much data
# body len in cm (float), dorsal fin len in cm (float),
	# identification integer Tigerstate0 = 0, Tigerstate1 = 1

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Check if data file exists
def check_file_exists(file_name):
	try:
		# Try opening the file to check if file valid, close file as to not leaving hanging data
		# os.path.isfile is a faster/more efficient check, but may lead to other issues, does not return r/w permissions
		# open() is a decent check here for a singular data file
		file = open(file_name, "r")
		file.close()
	except FileNotFoundError:
		file_name = input(file_name + " file not found. \nPlease try another file name (try adding the path): ")
		import_data_from_csv(file_name)
	except:
		print("Unknown error when retreiving data file.")

# Imports the data into a pandas dataframe
def import_data_from_csv(file_name):
	check_file_exists(file_name)

	panda_data = pd.read_csv(file_name, skiprows=[0], delimiter="\t",
							names=["airPollution", "waterPollution", "state"])

	return (panda_data)

# Plot the data
def plot_data_by_type(data, fileName):
	state0 = data.loc[data["state"]==0]
	state1 = data.loc[data["state"]==1]
	plt.scatter(state0["airPollution"], state0["waterPollution"], color="cyan", label="UT")
	plt.scatter(state1["airPollution"], state1["waterPollution"], color="orange", label="NV")

	# Plot meta data
	plt.xlabel("Air Pollution")
	plt.ylabel("Water Pollution")
	plt.title("Air Pollution vs. Water Pollution ")
	plt.legend(loc="lower right")
	plt.savefig(fileName)
	plt.show()

# Randomizes and returns the imported data
def randomize_data(data):
	# Drops record of previous data positions
	data = data.sample(frac=1).reset_index(drop=True)
	return(data)

# Normalize the data using standard deviation normalization
def normalize_data(data, normalize_columns=None):
	if normalize_columns == None:
		data=(data-data.mean())/data.std()
	else:
		for col in normalize_columns:
			data[col] = (data[col]-data[col].mean())/data[col].std()

	return(data)

# Divides provided data into 20/80 ratio for testing and training data
# For this data (size 300), divides into 60/240
def divide_data_into_training_test_txt(data):
	testing_len = int(len(data)*.2)
	# training_len = len(data)*.8
	test_set = data.iloc[:testing_len]
	training_set = data.iloc[testing_len:]

	np.savetxt("CompareState_training_set.txt", training_set, delimiter="\t", fmt="%g")
	np.savetxt("CompareState_test_set.txt", test_set, delimiter="\t", fmt="%g")
	
	# return test_set, training_set

def load_data_from_txt(fileName):
	return(pd.read_csv(fileName, delimiter='\t', 
		names=["air", "water", "state"]))

# Divides provided data into equal sized folds
def divide_training_into_fold_txts(data, num_of_folds):
	# Get the length of folds dependent on desired number of folds
	fold_len = int(len(data)/num_of_folds)

	# list_of_folds = []

	# Split provided data by desired number of folds, append to list
	for i in range(num_of_folds):
		validation_fold = data.iloc[fold_len*i:fold_len*(i+1)]
		train_fold_front = data.iloc[:fold_len*i]
		train_fold_back = data.iloc[fold_len*(i+1):]
		train_folds = pd.concat([train_fold_back, train_fold_front])

		np.savetxt("CompareState_Val_" + str(i) + ".txt", validation_fold, delimiter="\t", fmt="%g")		
		np.savetxt("CompareState_Train_" + str(i) + ".txt", train_folds, delimiter="\t", fmt="%g")		

# Input format: points as tuples: (pt1x, pt1y)
def euclidian_dist_between_2_pts_2_dimensions(point1, point2):
	return(np.sqrt((point1[0] - point2[0])**2 + (point1[1] - point2[1])**2))

# Generates lists of distances from training fold pts for each validation fold pt
# List format: [validation_pt_type, [distance tuple list]]
# Saves as a tuple: (training_pt_type, distance); Sorted by distance per pt
def get_sorted_pt_distances_for_each_test_pt(test_set, training_set):
	# Initialize empty list to hold all training fold point distances from each validation fold point
	# List to save validation pts in order
	test_set_distances = []

	# For each validation pt, get all distances from all training pts
	for i, valRow in test_set.iterrows():
		# Initialize list in which to save all distances between this validation pt and all training pts
		val_pt_distances = []
		valPt = (valRow["airPollution"], valRow["waterPollution"])

		for j, trainRow in training_set.iterrows():
			trainPt = (trainRow["airPollution"], trainRow["waterPollution"])

			# Get distance between validation and training points
			distance_bt_val_train = euclidian_dist_between_2_pts_2_dimensions(valPt, trainPt)

			# Save training point type, and distance from validation pt
			val_pt_distances.append((trainRow['state'], distance_bt_val_train))

		# Sort list by pt distance
		val_pt_distances = sorted(val_pt_distances, key=lambda x: x[1])

		# For all points in validation fold, return lists of training point types sorted by distance
		test_set_distances.append([valRow["state"], val_pt_distances])

	return test_set_distances

def predicted_type_from_majority(value_list):
	state0 = value_list.count(0)
	state1 = value_list.count(1)

	if state0 > state1:
		return (0)
	elif state1 > state0:
		return (1)

def plot_accuracy_of_ks(misclassification_rates_summed, all_validation_data_len):
	val_k_x_axis = []
	accuracy_y_axis = []

	for i in range(len(misclassification_rates_summed)):
		if i % 2 == 0:
			continue

		error = misclassification_rates_summed[i]
		accuracy = (1 - (error/all_validation_data_len)) * 100
		val_k_x_axis.append(i)
		accuracy_y_axis.append(accuracy)

	plt.plot(val_k_x_axis, accuracy_y_axis, marker="o")

	# Plot meta data
	plt.xlabel("Value of k for kNN")
	plt.ylabel("Cross Validated Accuracy (%) ")
	plt.title("Air vs. Water pollution for types of States")
	plt.savefig("CompareState_accuracy.png")
	plt.show()

def best_k_from_K_fold_cross_validation(max_k, num_of_folds):
	# If max_k is even, subtract one; we only check to the highest odd number
	if max_k % 2 == 0:
		max_k = max_k - 1

	# List for collecting sum misclassification rates per k
	# Will be populated by sums from k_misclassification_rate_lists
	k_misclassification_rate = [0] * (max_k+1)

	# List of lists that contain misclassification rates per k per fold
	k_misclassification_rate_lists = [ [0] * (max_k+1) for i in range(num_of_folds)]

	# For each fold, load up appropriate validation and training set
	for f in range(num_of_folds):
		# Import fold data
		training_fold = load_data_from_txt("CompareState_Train_" + str(f) + ".txt")
		validation_fold = load_data_from_txt("CompareState_Val_" + str(f) + ".txt")

		# Generates lists of distances from training fold pts for each validation fold pt
		# List format: [validation_pt_type, [distance tuple list]]
		# Saves as a tuple: (training_pt_type, distance); Sorted by distance per pt
		validation_fold_distances = get_sorted_pt_distances_for_each_test_pt(validation_fold, training_fold)

		for val_pt in range(len(validation_fold_distances)):
			val_pt_type = validation_fold_distances[val_pt][0]
			val_distances = validation_fold_distances[val_pt][1]

			nearby_pt_types = []

			for k in range(len(val_distances)):
				# If loop has checked until maximum k desired, break out
				if k >= max_k+1:
					break

				# For each point, starting with the closest, save its type
				nearby_pt_types.append(val_distances[k][0])

				# Do not check even ks (1,3,5 due to starting from index 0)
				if k % 2 != 0:
					continue

				# Predict the validation point type from the majority of k nearby value types
				predicted_type = predicted_type_from_majority(nearby_pt_types)

				# If the prediction is wrong, add to the error measure
				if predicted_type != val_pt_type:
					k_misclassification_rate_lists[f][k+1] = k_misclassification_rate_lists[f][k+1] + 1

	# For each k, find the sum of errors, save in k_misclassification_rate
	for k in range(max_k+1):
		misclassification_rate = 0
		for fold in range(num_of_folds):
			misclassification_rate = misclassification_rate + k_misclassification_rate_lists[fold][k]

		k_misclassification_rate[k] = misclassification_rate

	plot_accuracy_of_ks(k_misclassification_rate, len(validation_fold_distances)*num_of_folds)

	# Replace even ks in list with "None" instead of 0
	new = [x if x != 0 else None for x in k_misclassification_rate]

	# Get smallest average misclassification rate
	best_k = new.index(min((x for x in new if x is not None)))

	return (best_k)

def get_kNN_orig_and_predicted_type_for_test_vals(test_set, training_set, best_k):
	# Get distance from each test set point to all other points sorted by closest
	test_set_pt_distances = get_sorted_pt_distances_for_each_test_pt(test_set, training_set)

	# For each list of training pts by distance, shorten to best_k length
	k_truncated_pt_distances = [None] * len(test_set_pt_distances)
	for i in range(len(test_set_pt_distances)):
		k_truncated_pt_distances[i] = [test_set_pt_distances[i][0], test_set_pt_distances[i][1][:best_k]]

	# Initialize empty list to save actual test pt values and predicted values
	orig_and_predicted_types = [None] * len(k_truncated_pt_distances)

	# Create list that stores tuples of (actual fish type, predicted fish type) per test pt
	for test_pt in range(len(k_truncated_pt_distances)):
		test_pt_type = int(k_truncated_pt_distances[test_pt][0])
		test_distances = k_truncated_pt_distances[test_pt][1]

		nearby_pt_types = [int(i[0]) for i in test_distances]
		predicted_type = predicted_type_from_majority(nearby_pt_types)

		orig_and_predicted_types[test_pt] = (test_pt_type, predicted_type)

	return(orig_and_predicted_types)

def test_set_with_best_k(test_set, training_set, best_k):
	# test_set_pt_distances = get_sorted_pt_distances_for_each_test_pt(test_set, training_set, best_k)
	# List of tuples of (correct test type, predicted test type)
	test_vals_type_predictions = get_kNN_orig_and_predicted_type_for_test_vals(test_set, training_set, best_k)

	# Initialize counters
	true_positive = 0
	true_negative = 0
	false_positive = 0
	false_negative = 0

	# Set positives to fishtype 1, negatives to 0
	positive = 1
	negative = 0

	test_pt_validity = [None] * len(test_set)
	
	# Count confusion matrix values
	for i in range(len(test_vals_type_predictions)):
		actual_type = test_vals_type_predictions[i][0]
		predicted_type = test_vals_type_predictions[i][1]
		if actual_type == positive and predicted_type == positive:
			true_positive = true_positive + 1
			test_pt_validity[i] = "TP"
		elif actual_type == negative and predicted_type == negative:
			true_negative = true_negative + 1
			test_pt_validity[i] = "TN"
		elif actual_type == negative and predicted_type == positive:
			false_positive = false_positive + 1
			test_pt_validity[i] = "FP"
		elif actual_type == positive and predicted_type == negative:
			false_negative = false_negative + 1
			test_pt_validity[i] = "FN"

	test_set["validity"] = test_pt_validity

	# Calculate measures
	accuracy = (true_positive + true_negative)/(true_positive + true_negative + false_positive + false_negative)
	precision = true_positive / (true_positive + false_positive)
	recall = true_positive / (true_positive + false_negative)
	f1 = 2 * (1/((1/precision) + (1/recall)))

	# print measures
	print("True Positives: ", true_positive)
	print("True Negatives: ", true_negative)
	print("False Positives: ", false_positive)
	print("False Negatives: ", false_negative)
	print("")
	print("Accuracy: ", accuracy)
	print("Precision: ", precision)
	print("Recall: ", recall)
	print("F1: ", f1)

	training_state0 = training_set.loc[training_set["state"]==0]
	training_state1 = training_set.loc[training_set["state"]==1]
	plt.scatter(training_state0["airPollution"], training_state0["waterPollution"], color="cyan", label="Tigerstate0_Training", marker=".")
	plt.scatter(training_state1["airPollution"], training_state1["waterPollution"], color="orange", label="Tigerstate1_Training", marker=".")

	test_state0 = test_set.loc[test_set["state"]==0]
	test_state1 = test_set.loc[test_set["state"]==1]

	test_TP0 = test_state0.loc[test_state0["validity"]=="TP"]
	test_TN0 = test_state0.loc[test_state0["validity"]=="TN"]
	test_FP0 = test_state0.loc[test_state0["validity"]=="FP"]
	test_FN0 = test_state0.loc[test_state0["validity"]=="FN"]

	test_TP1 = test_state1.loc[test_state1["validity"]=="TP"]
	test_TN1 = test_state1.loc[test_state1["validity"]=="TN"]
	test_FP1 = test_state1.loc[test_state1["validity"]=="FP"]
	test_FN1 = test_state1.loc[test_state1["validity"]=="FN"]

	color = ["red","blue"]

	# plt.scatter(test_TP0["airPollution"], test_TP0["waterPollution"], color="cyan", marker="P", label="True Positives_TF0", edgecolor="blue")
	plt.scatter(test_TP1["airPollution"], test_TP1["waterPollution"], color="orange", marker="P", label="True Positives_TF1", edgecolor="red")

	plt.scatter(test_TN0["airPollution"], test_TN0["waterPollution"], color="cyan", marker="X", label="True Negatives_TF0", edgecolor="blue")
	# plt.scatter(test_TN1["airPollution"], test_TN1["waterPollution"], color="orange", marker="X", label="True Negatives_TF1", edgecolor="red")

	plt.scatter(test_FP0["airPollution"], test_FP0["waterPollution"], color="cyan", marker="P", label="False Positives_TF0", edgecolor="black")
	# plt.scatter(test_FP1["airPollution"], test_FP1["waterPollution"], color="blue", marker="P", label="False Positives_TF1", edgecolor="black")

	# plt.scatter(test_FN0["airPollution"], test_FN0["waterPollution"], color="red", marker="X", label="False Negatives_TF0", edgecolor="black")
	plt.scatter(test_FN1["airPollution"], test_FN1["waterPollution"], color="orange", marker="X", label="False Negatives_TF1", edgecolor="black")

	# Plot meta data
	plt.xlabel("Body Length")
	plt.ylabel("Fin Length")
	plt.title("Testing Data Validity (True and False Postiives and Negatives)")
	plt.legend(loc="upper left")
	plt.savefig("CompareState_validity.png")
	plt.show()


def main():
	file_name = "UTvsNV.txt"
	max_k = 40
	num_of_folds = 5

	file_name = str(input("Raw fish data file name: "))
	max_k = int(input("Maximum k to test until: "))
	# num_of_folds = int(input("Number of folds to divide training set into: "))

	# Import data from CSV
	# print("Importing data from original file: ", file_name)
	fishData = import_data_from_csv(file_name)

	# Plot raw data
	plot_data_by_type(fishData, "CompareState_all_data.png")

	# Randomize data order
	# print("Randomizing data...")
	fishData = randomize_data(fishData)
	# Normalize airPollution and waterPollution columns by standard deviation
	# print("Normalizing data by the standard deviation...")
	fishData = normalize_data(fishData, ["airPollution", "waterPollution"])

	# plot_data_by_type(fishData, "CompareState_normalized_data.png")
	
	# Divide data into training and testing sets, save them as txts
	# print("Dividing data into training and test sets; \n\tsaving them as txt files: CompareState_[test or training].txt")
	divide_data_into_training_test_txt(fishData)

	# Load up txts
	# print("Loading training and test sets from txt files...")
	training_set = load_data_from_txt("CompareState_training_set.txt")
	test_set = load_data_from_txt("CompareState_test_set.txt")

	# Divide training set into folds (5) and save them as Validation and Testing txts
	# print("Dividing training data into folds; \n\tsaving them as txt files: CompareState_[Val or Train]_#.txt")
	divide_training_into_fold_txts(training_set, num_of_folds)

	# Retrieve the best k using K fold cross validation
	best_k = best_k_from_K_fold_cross_validation(max_k, num_of_folds)
	# best_k = 9
	print("\nBest k: ", best_k, "\n")

	# Use the best k on the saved test set, prints out measures on quality
	test_set_with_best_k(test_set, training_set, best_k)

main()
