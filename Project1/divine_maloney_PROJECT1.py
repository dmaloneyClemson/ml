#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  09 10:14:27 2019

@author: Divine Maloney
Project 1: k-Nearest Neighbor


Assignment: Develop a k-Nearest Neighbor algorithm that will predict the
species of a fish. Use a test set and 5-fold cross validation to determin the
best number of neight to use in your prediction. Use a confusion matrix to help
evaluate the results.  Used this for learning how to implement knn  https://machinelearningmastery.com/tutorial-to-implement-k-nearest-neighbors-in-python-from-scratch/ 
and https://stackabuse.com/k-nearest-neighbors-algorithm-in-python-and-scikit-learn/ to do the graphs.
and for confusion matrix i used https://stackoverflow.com/questions/2148543/how-to-write-a-confusion-matrix-in-python
https://www.dataschool.io/simple-guide-to-confusion-matrix-terminology/
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
import operator
import itertools


def loadplotDataSet(filename, split):
    
    fishData = pd.read_csv(filename,skiprows=[0], delimiter='\t', 
                         names=['Body Length', 
                                'Dorsal Fin Length',  
                                'Type'])
    TigerFish1 = fishData.loc[fishData.Type == 1]
    TigerFish0 = fishData.loc[fishData.Type == 0]  
    #Preparing plot
    fig = plt.figure()
    fish = fig.add_subplot()  
    fish1 = fish.scatter(TigerFish1['Body Length'],TigerFish1['Dorsal Fin Length'], 
                        marker="^",color="purple")
    fish0 = fish.scatter(TigerFish0['Body Length'],TigerFish0['Dorsal Fin Length'],
                           marker=".",color="orange")
    fish.set(title="Body Length vs Dorsal Fin Length", 
                   xlabel="Body Length", 
                   ylabel="Dorsal Fin Length")
    fish.legend([fish1, fish0], 
                ["TigerFish1", "TigerFish0"]
                , loc='best')
    plt.show()

    # uses pandas library to read a tab-delimited text file from user input
    # headers are removed
    ffData = pd.read_csv(filename, delimiter='\t',header=None)
    fishDataSet = ffData.loc[1:]
    fishDataSet = fishDataSet.sample(frac=1).reset_index(drop=True)
    
    # ffData is split depending on input an 80 on split gives 80-20. 
    # trainingSet gets 80% and testSet get 20%
    trainingSet, testSet, = np.split(fishDataSet, [int(split*len(fishDataSet))])
    
    # saves trainingSet and testSet into two text files
    np.savetxt("TrainingSet.txt", trainingSet, fmt='%g', delimiter='\t')
    np.savetxt("TestSet.txt", testSet, fmt='%g', delimiter='\t')
    
    # Intiates set at 5. splits the trainingSet into 5 with equal number of 
    # elements
    sets = 5
    TrainingData_split = np.array_split(trainingSet, sets)
    # for loop that iterates from 5 decrement, creates a file reverse order
    # named. TrainingData_split[1] data goes to Val 5, 2 to 4, and so on.
    for i in reversed(range(sets)):
        np.savetxt('Val' + str(sets-i) + '.txt',TrainingData_split[i], 
                   fmt='%g', delimiter='\t')
    
    # creates list of iteration of 1,2,3,4,5 in 4 elements
    # {(1,2,3,4),(1,2,3,4),(1,2,4,5),(1,3,4,5),(2,3,4,5)}
    trainSets = itertools.combinations([1,2,3,4,5],sets-1)
    trainSet_ = list(trainSets)
    
    # goes through the list created above and appends corresponding split for
    # each set and recursively saves it into a text file.
    for x in range(len(trainSet_)):
        appended_data = []
        for y in range(len(trainSet_[x])):
            n = trainSet_[x][y]
            appended_data.append(TrainingData_split[n-1])
        appended_data = pd.concat(appended_data)
        np.savetxt("Train" + str(x+1) + ".txt", appended_data, fmt='%g', 
                   delimiter='\t')
    
    return(trainingSet, testSet)
    
# Uses pythagorean theorem to determine the distance between two points
def euclideanDistance(instance1, instance2, length):
    distance = 0
    for x in range(length):
        distance += pow((float(instance1[x]) - float(instance2[x])),2)
    return (math.sqrt(distance))

"""
 gets the k closests points in between testInstance and trainingSet
 INPUT: trainingSet: dataframe, testInstance: specific row in the test df
 OUTPUT: dataframe that contains k elements
"""
def getNeighbors(trainingSet, testInstance,k):
    distances = []
    length    = len(testInstance) - 1
    for x in range(len(trainingSet)):
        dist = euclideanDistance(testInstance, trainingSet.loc[x],length)
        distances.append((trainingSet.loc[x],dist))
    distances.sort(key=operator.itemgetter(1))
    
    neighbors = []
    for x in range (k):
        neighbors.append(distances[x][0])
    neighbors_df = pd.DataFrame(neighbors)
    return neighbors_df

"""
 tallies the type of the k neighbors and returns the type that has the most
 votes
 INPUT: types column of the neighbors dataframe
 OUTPUT: resulting type depending on the 'winner' of the votes
"""
def getResponse(neighbors_typeList):
    classVotes = {}
    for x in range(len(neighbors_typeList)):
        response = neighbors_typeList[x]
        if response in classVotes:
            classVotes[response] += 1
        else:
            classVotes[response] = 1
    
    sortedVotes = sorted(classVotes.items(), key=operator.itemgetter(1), reverse = True)
    return sortedVotes[0][0]

"""
 counts accuracy % by counting correct predicions and dividing it with the 
 length of the testSet and multiplying by 100
"""
def getAccuracy(testSet, predictions):
    correct = 0
    for x in range(len(testSet)):
        if testSet.iloc[x,-1] == predictions[x]:
            correct += 1
    return(correct/float(len(testSet))) * 100

def getError(testSet, predictions):
    incorrect = 0
    for x in range(len(testSet)):
        if testSet.iloc[x,-1] != predictions[x]:
            incorrect += 1
    return incorrect


def errPlot(errorDF):
    errorDF.loc["TOTAL"] = errorDF.sum()
    y = errorDF.loc["TOTAL"]    
    fig = plt.figure()
    error = fig.add_subplot()    
    error.plot(y, marker="o",color="Purple", markerfacecolor="Orange",
                       linestyle = "dashed")
    error.locator_params(integer=True)
    plt.xlabel("Value of k for kNN", axes=error)
    plt.ylabel("Error", axes=error)
    plt.title("# of Missclassifications")
    plt.show()

def accPlot(accuracyDF):
    accuracyDF.loc["MEAN"] = accuracyDF.mean()
    y = accuracyDF.loc["MEAN"]
    fig = plt.figure()
    accuracy = fig.add_subplot()    
    accuracy.plot(y, marker='o', color='Purple', markerfacecolor='Orange',
                  linestyle='dashed')
    accuracy.locator_params(integer = True)
    plt.xlabel("Value of K for kNN",axes=accuracy)
    plt.ylabel("Cross-Validated Accuracy",axes=accuracy)
    plt.title("% ACCURACY")
    plt.show()

def validate(trainSet, testSet,k):
    predictions = []
    for x in range(len(testSet)):
        neighbors_df = getNeighbors(trainSet, testSet.loc[x],k)
        neighbors_typeList = neighbors_df.iloc[:,-1].values.tolist()
        result = getResponse(neighbors_typeList)
        predictions.append(result)
    accuracy = getAccuracy(testSet, predictions)
    error = getError(testSet, predictions)
    print(accuracy,error)
    print("acccuracy^^ error^^")

    return accuracy, error

def validationTest():

        errorDF = pd.DataFrame()
        accuracyDF = pd.DataFrame()
        tryK = [1,3,5,7,9,15]
        for x in  range(5):
            for k in tryK:
                print("Train" + str(x) + ".txt and Val" + str(x) + ".txt against k="+str(k))
                trainSet = pd.read_csv("Train"+str(x+1)+".txt", delimiter='\t', header=None)
                testSet = pd.read_csv("Val"+str(x+1)+".txt", delimiter='\t', header=None)
                accuracy, error = validate(trainSet, testSet, k)
                errorDF.at[x,k] = error
                accuracyDF.at[x,k] = accuracy
        errPlot(errorDF)
        accPlot(accuracyDF)
        
    
def tester(trainSet, testSet,k):
        validate(trainSet, testSet, k)
        while True:
            bodyLength = float(input("Input Body Length: "))
            dorsalLength = float(input("Input Dorsal Fin Length: "))
            if bodyLength == 0 and dorsalLength == 0:
                break
            elif bodyLength < 0 or dorsalLength < 0:
                print("Lengths needs to be positive values.")
            else:
                lengthSet = [bodyLength, dorsalLength]
                input_neighbors = getNeighbors(trainSet, lengthSet,k)
                input_neighbors_typeList = input_neighbors.iloc[:,-1].values.tolist()
                result = getResponse(input_neighbors_typeList)
                print("That fish is predicted to be: TigerFish"+str(int(result)))

def intro(filename,split):
    loadplotDataSet(filename,split)
    print("""This is my KNN project1, you can take a look at the data plotted above:) 
    if you'd like to see the plots of my data...i.e Accuracy&Error, type = 'yes', if you'd like to 
    just test the body length and dorsal fin length type 'no', give it a few seconds after you have
    typed yes or no.
    """)   
    yes = {'yes','y'}
    choice = input().lower()
    
    if choice in yes:
        validationTest()
    else:
        return
    
def main():
    filename = input("What is the filename: ")
    split = 0.80
    intro(filename,split)
    k=7
    trainSet = pd.read_csv("TrainingSet.txt", delimiter='\t', header=None)
    testSet = pd.read_csv("TestSet.txt", delimiter='\t', header=None)   
    tester(trainSet,testSet,k)
 


main()