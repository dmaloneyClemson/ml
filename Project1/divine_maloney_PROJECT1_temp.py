#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  09 10:14:27 2019

@author: Divine Maloney
Project 1: k-Nearest Neighbor


Assignment: Develop a k-Nearest Neighbor algorithm that will predict the
species of a fish. Use a test set and 5-fold cross validation to determin the
best number of neight to use in your prediction. Use a confusion matrix to help
evaluate the results.  

Used this for learning how to implement knn  https://machinelearningmastery.com/tutorial-to-implement-k-nearest-neighbors-in-python-from-scratch/ 
and https://stackabuse.com/k-nearest-neighbors-algorithm-in-python-and-scikit-learn/ to do the graphs.
and for confusion matrix i used https://stackoverflow.com/questions/2148543/how-to-write-a-confusion-matrix-in-python
https://www.dataschool.io/simple-guide-to-confusion-matrix-terminology/

Steps used:
    -randomize fish data
    -divide two files into training and testing
    -split into 5 groups
    -set up validation/training groups
    -write python knn
    -use picked k to test on set
"""


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import operator
import itertools


#Load the data in and plots the data
def loadplotDataSet(filename, split):
    
    fishData = pd.read_csv(filename,skiprows=[0], delimiter='\t', 
                         names=['Body Length', 
                                'Dorsal Fin Length',  
                                'Type'])
    TigerFish1 = fishData.loc[fishData.Type == 1]
    TigerFish0 = fishData.loc[fishData.Type == 0]  
   
    fig = plt.figure()
    fish = fig.add_subplot()  
    fish1 = fish.scatter(TigerFish1['Body Length'],
                        TigerFish1['Dorsal Fin Length'], 
                        color="purple")
    fish0 = fish.scatter(TigerFish0['Body Length'],
                         TigerFish0['Dorsal Fin Length'],
                         color="orange")
    fish.set(title="Body Length vs Dorsal Fin Length", 
                   xlabel="Body Length", 
                   ylabel="Dorsal Fin Length")
    fish.legend([fish1, fish0], 
                ["TigerFish1", "TigerFish0"]
                , loc='best')
    plt.show()
#read in raw data from file and create a dataframe
    rawfishData = pd.read_csv(filename, delimiter='\t',header=None)
    fishDataSet = rawfishData.loc[1:]
    fishDataSet = fishDataSet.sample(frac=1).reset_index(drop=True)
    
    # split the data into training and test 
    trainingBatch, testBatch, = np.split(fishDataSet, [int(split*len(fishDataSet))])
    np.savetxt("trainingBatch.txt", trainingBatch, fmt='%g', delimiter='\t')
    np.savetxt("testBatch.txt", testBatch, fmt='%g', delimiter='\t')
    
    # Splits those batches into 5 data points
    sets = 5
    TrainingData_split = np.array_split(trainingBatch, sets)
    # creates a file reverse orderd
    for i in reversed(range(sets)):
        np.savetxt('Val' + str(sets-i) + '.txt',TrainingData_split[i], 
                   fmt='%g', delimiter='\t')
    
    # creates list of iteration of 1,2,3,4,5 in 4 elements
    # {(1,2,3,4),(1,2,3,4),(1,2,4,5),(1,3,4,5),(2,3,4,5)}
    trainBatchs = itertools.combinations([1,2,3,4,5],sets-1)
    trainBatch_ = list(trainBatchs)
    
    #The list is created and appends to corresponding split
    for x in range(len(trainBatch_)):
        appended_data = []
        for y in range(len(trainBatch_[x])):
            n = trainBatch_[x][y]
            appended_data.append(TrainingData_split[n-1])
        appended_data = pd.concat(appended_data)
        np.savetxt("Train" + str(x+1) + ".txt", appended_data, fmt='%g', 
                   delimiter='\t')
    
    return(trainingBatch, testBatch)
    
# euclideanDistanct function to determine the distance between two points
def euclideanDistance(instance1, instance2, length):
    distance = 0
    for x in range(length):
        distance += pow((float(instance1[x]) - float(instance2[x])),2)
    return (math.sqrt(distance))

"""
 gets the k closests points in between testInstance and trainingBatch
 INPUT: trainingBatch: dataframe, testInstance: specific row in the test df
 OUTPUT: dataframe that contains k elements
"""
def getNeighbors(trainingBatch, testInstance,k):
    distances = []
    length    = len(testInstance) - 1
    for x in range(len(trainingBatch)):
        dist = euclideanDistance(testInstance, trainingBatch.loc[x],length)
        distances.append((trainingBatch.loc[x],dist))
    distances.sort(key=operator.itemgetter(1))
    
    neighbors = []
    for x in range (k):
        neighbors.append(distances[x][0])
    neighbors_df = pd.DataFrame(neighbors)
    return neighbors_df

"""
 counts the type of the k neighbors and returns the type that has the most votes
 INPUT: types column of the neighbors dataframe
 OUTPUT: resulting type depending on the 'winner' of the votes
"""
def getResponse(neighbors_typeList):
    classVotes = {}
    for x in range(len(neighbors_typeList)):
        response = neighbors_typeList[x]
        if response in classVotes:
            classVotes[response] += 1
        else:
            classVotes[response] = 1
    
    sortedVotes = sorted(classVotes.items(), key=operator.itemgetter(1), reverse = True)
    return sortedVotes[0][0]

#counts accuracy % by counting correct predicions and dividing it with the 
def getAccuracy(testBatch, predictions):
    correct = 0
    for x in range(len(testBatch)):
        if testBatch.iloc[x,-1] == predictions[x]:
            correct += 1
    return(correct/float(len(testBatch))) * 100

#gets the error by 
def getError(testBatch, predictions):
    incorrect = 0
    for x in range(len(testBatch)):
        if testBatch.iloc[x,-1] != predictions[x]:
            incorrect += 1
    return incorrect

#plot Error
def errPlot(errorDF):
    errorDF.loc["TOTAL"] = errorDF.sum()
    y = errorDF.loc["TOTAL"]    
    fig = plt.figure()
    error = fig.add_subplot()    
    error.plot(y, marker="o",color="Purple", markerfacecolor="Orange",
                       linestyle = "dashed")
    error.locator_params(integer=True)
    plt.xlabel("Value of k for kNN", axes=error)
    plt.ylabel("Error", axes=error)
    plt.title("# of Missclassifications")
    plt.show()

#plot Accuracy
def accPlot(accuracyDF):
    accuracyDF.loc["MEAN"] = accuracyDF.mean()
    y = accuracyDF.loc["MEAN"]
    fig = plt.figure()
    accuracy = fig.add_subplot()    
    accuracy.plot(y, marker='o', color='Purple', markerfacecolor='Orange',
                  linestyle='dashed')
    accuracy.locator_params(integer = True)
    plt.xlabel("Value of K for kNN",axes=accuracy)
    plt.ylabel("Cross-Validated Accuracy",axes=accuracy)
    plt.title("% ACCURACY")
    plt.show()

#validates based on sets and spits out accuracy & error
def validate(trainBatch, testBatch,k):
    predictions = []
    for x in range(len(testBatch)):
        neighbors_df = getNeighbors(trainBatch, testBatch.loc[x],k)
        neighbors_typeList = neighbors_df.iloc[:,-1].values.tolist()
        result = getResponse(neighbors_typeList)
        predictions.append(result)
    accuracy = getAccuracy(testBatch, predictions)
    error = getError(testBatch, predictions)
    print(accuracy,error)
    print("acccuracy^^ error^^")

    return accuracy, error

#picked random odd values 1-15 to validate 
def validationTest():

        errorDF = pd.DataFrame()
        accuracyDF = pd.DataFrame()
        tryK = [1,3,5,7,9,15]
        for x in  range(5):
            for k in tryK:
                print("Train" + str(x) + ".txt and Val" + str(x) + ".txt against k="+str(k))
                trainBatch = pd.read_csv("Train"+str(x+1)+".txt", delimiter='\t', header=None)
                testBatch = pd.read_csv("Val"+str(x+1)+".txt", delimiter='\t', header=None)
                accuracy, error = validate(trainBatch, testBatch, k)
                errorDF.at[x,k] = error
                accuracyDF.at[x,k] = accuracy
        errPlot(errorDF)
        accPlot(accuracyDF)
        
def get_confusion_matrix_values(y_true, y_pred):
    cm = confusion_matrix(y_true, y_pred)
    return(cm[0][0], cm[0][1], cm[1][0], cm[1][1])

TP, FP, FN, TN = get_confusion_matrix_values(x_test, x_pred)
        
#takes in the training batch and test batch and the value of k and prompts the users
#for the body length and dorsal fin for each fit.
def tester(trainBatch, testBatch,k):
        validate(trainBatch, testBatch, k)
        while True:
            bodyLength = float(input("Input Body Length: "))
            dorsalLength = float(input("Input Dorsal Fin Length: "))
            if bodyLength == 0 and dorsalLength == 0:
                break
            elif bodyLength < 0 or dorsalLength < 0:
                print("Lengths needs to be positive values.")
            else:
                lengthSet = [bodyLength, dorsalLength]
                input_neighbors = getNeighbors(trainBatch, lengthSet,k)
                input_neighbors_typeList = input_neighbors.iloc[:,-1].values.tolist()
                result = getResponse(input_neighbors_typeList)
                print("That fish is predicted to be: TigerFish"+str(int(result)))

def intro(filename,split):
    loadplotDataSet(filename,split)
    print("""This is my KNN project1, you can take a look at the data plotted above:) 
    if you'd like to see the plots of my data...i.e Accuracy&Error, type = 'yes', if you'd like to 
    just test the body length and dorsal fin length type 'no', give it a few seconds after you have
    typed yes or no.
    """)   
    yes = {'yes','y'}
    choice = input().lower()
    
    if choice in yes:
        validationTest()
    else:
        return
    
def main():
    filename = input("What is the filename: ")
    split = 0.80
    intro(filename,split)
    k=9
    trainBatch = pd.read_csv("trainingBatch.txt", delimiter='\t', header=None)
    testBatch = pd.read_csv("testBatch.txt", delimiter='\t', header=None)   
    tester(trainBatch,testBatch,k)
 


main()