import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from statistics import mean, median

global make_plots
global print_values
make_plots = False
print_values = False

# Check if data file exists
def check_file_exists(file_name, colNames):
	try:
		file = open(file_name, "r")
		file.close()
	except:
		print("No file named GPAdata.txt")

# Imports the data into a pandas dataframe
def importData(file_name, colNames):
	check_file_exists(file_name, colNames)

	panda_data = pd.read_csv(file_name, 
                            skiprows=[0],
                            delimiter="\t",
							names=colNames)
	return (panda_data)

# Plot the data 
def plotData(data):
	rawData = pd.read_csv("FF43.txt",
                        skiprows=[0], 
                        delimiter='\t', 
		            names=['timPerWeek', 
				            'beerPerWeek',  
				            'gpa'])

	timeWeek = rawData['timPerWeek'].values
	beerWeek = rawData['beerPerWeek'].values
	gpaScores = rawData['gpa'].values

	fig = plt.figure()
	gpa = fig.add_subplot()  

	gpaTime = gpa.scatter(timeWeek,
				gpaScores, 
				color="orange")
	gpaBeer = gpa.scatter(beerWeek,
				gpaScores,
				color="purple")

	gpa.set(title="TimeSpent Studying vs Beer Drinking Per Week", 
	xlabel="TimeSpent Studying", 
	ylabel="Beer Drinking Per Week")
	plt.show()


def randomizeData(data):
	data = data.sample(frac=1).reset_index(drop=True)
	return(data)

# using standard deviation to normalize all the data 
def normData(data, normColumns=None):
	if normColumns == None:
		data=(data-data.mean())/data.std()
	else:
		for col in normColumns:
			data[col] = (data[col]-data[col].mean())/data[col].std()
	return(data)

# splits data into 80/20 testing and training data
def splitData(data, testing_size=.2):
	testing_len = int(len(data)*testing_size)
	testSet = data.iloc[:testing_len]
	trainingSet = data.iloc[testing_len:]

	np.savetxt("trainingSet.txt", trainingSet, delimiter="\t", fmt="%g")
	np.savetxt("testSet.txt", testSet, delimiter="\t", fmt="%g")
	

def loadData(fileName, colNames):
	return(pd.read_csv(fileName, delimiter='\t', 
		names=colNames))

# predicts the appropriate y value from x1, x2
def hypothesisFunction(x1, x2, weights):
	return (weights[0] 
		 + (weights[1] * x1)
		 + (weights[2] * x2)
		 + (weights[3] * x1 * x2) 
		 + (weights[4] * (x1 ** 2))
		 + (weights[5] * (x2 ** 2)))


# Given the dataset and a list of weights, calculates the error cost J
# Does this by summing squared errors for each dataset, then dividing by two & len of the dataset
def JCost(data, weights):
	m = len(data)
	cost_func_sum = 0
	for i, row in data.iterrows():
		x1 = row[0]
		x2 = row[1]
		y = row[2]

		cost_func_sum = cost_func_sum + ((hypothesisFunction(x1, x2, weights) - y)**2)

	# print("cost: ", ((1/(2*m)) * cost_func_sum))
	return ((1/(2*m)) * cost_func_sum)

# To aid in updating weights
# Given the index of w (whether its w0, w1, w2, etc), returns the proper x val
def get_x_n(w, x1, x2):
	if w == 0:
		return 1
	elif w == 1:
		return x1
	elif w == 2:
		return x2
	elif w == 3:
		return x1 * x2
	elif w == 4:
		return x1**2
	elif w == 5:
		return x2**2

# Updates the w values using gradient decent
def updateWeights(data, weights, alpha):
	# Empty list to store the updated weights in
	new_weights = [None] * len(weights) 
	for w in range(len(weights)):
		m = len(data)
		w_old = weights[w]
		w_temp = 0
		for i, row in data.iterrows():
			x1 = row[0]
			x2 = row[1]
			y = row[2]

			# Get the x value to multiply at the end when calculating new w
			x_n = get_x_n(w, x1, x2)
			w_temp = w_temp + (hypothesisFunction(x1, x2, weights) - y) * x_n

		w_new = w_old - alpha * (1/m) * w_temp
		new_weights[w] = w_new
	return new_weights

# Plot the costs over iterations
def plotCost(iteration, costs):
	plot_low = 0
	plot_high = len(iteration)
    

def bestWeights(iterations, data, weights, alpha):
	# Calculates cost of updated weights, and updates weights until max number of iterations

	costs = []
	iteration = []
	for i in range(iterations):
		weights = updateWeights(data, weights, alpha)
		cost = JCost(data, weights)
		costs.append(cost)
		iteration.append(i)

	plotCost(iteration, costs)

	return weights, costs[-1]


def getMeanStd(data, colNames):
	means = {}
	stds = {}
	for col in colNames:
		means[col] = data[col].mean()
		stds[col] = data[col].std()
	return means, stds

def normalize(x, data_mean, data_std):
	N = (x-data_mean)/data_std
	return N

def userTest(weights, meanData, data_stds):
	print("\n")
	print("Enter the values.")
	try:
		xStudy = int(input("Studying Min per Week: "))
		xBeer = int(input("Beer Oz per Week: "))
	except ValueError:
		print("Please enter a valid number.")
		userTest(meanData, data_stds)
	except:
		print("Unknown error. Please try again.")
		userTest(meanData, data_stds)

	if xStudy == 0 and xBeer == 0:
		return

	xStudy = normalize(xStudy, meanData["studying_min"], data_stds["studying_min"])
	xBeer = normalize(xBeer, meanData["beer_oz"], data_stds["beer_oz"])
    
	y = hypothesisFunction(xStudy, xBeer, weights)
	print("GPA:", y)
	userTest(weights, meanData, data_stds)
    
def testing(data, x_cols, weights):
	meanData, data_stds = getMeanStd(data, x_cols)
	userTest(weights, meanData, data_stds)

def main():
	file_name = "GPAData.txt"
	alpha = .2
	weights = [1] * 6
	iterations = 80
	colNames = ["studying_min", "beer_oz", "gpa"]
	studyingStr = colNames[0]		#x1
	beerStr = colNames[1]			#x2
	gpaStr = colNames[2]			#y				

	gpa_data = importData(file_name, colNames)
	mode = 1
	print("\nimplementing the algorithm, please wait.\n")
	gpaRandom = randomizeData(gpa_data)
	gpaNorm = normData(gpaRandom, [studyingStr, beerStr])
	splitData(gpaNorm, .3)
	trainingSet = loadData("trainingSet.txt", colNames)
	testSet = loadData("testSet.txt", colNames)
	weights, JFinal = bestWeights(iterations, trainingSet, weights, alpha)
	
	if mode == 1:
		testing(gpa_data, ["studying_min", "beer_oz"], weights)


main()
