# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 16:47:58 2019

@author: divin
"""
import statistics 
import matplotlib.pyplot as plt
import pandas as pd
plt.rcParams['figure.figsize'] = (10.0, 8.0)
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


def plotRaw():
		
	rawData = pd.read_csv("GPAdata.txt",skiprows=[0], delimiter='\t', 
		names=['timPerWeek', 
				'beerPerWeek',  
				'gpa'])

	timeWeek = rawData['timPerWeek'].values
	beerWeek = rawData['beerPerWeek'].values
	gpaScores = rawData['gpa'].values

	fig = plt.figure()
	gpa = fig.add_subplot()  

	#plt.plot(x, y, color='#58b970', label='Regression Line')

	gpaTime = gpa.scatter(timeWeek,
				gpaScores, 
				color="orange")
	gpaBeer = gpa.scatter(beerWeek,
				gpaScores,
				color="purple")

	gpa.set(title="TimeSpent Studying vs Beer Drinking Per Week", 
	xlabel="TimeSpent Studying", 
	ylabel="Beer Drinking Per Week")
	plt.show()



def randomize_data(data):
	# Drops record of previous data positions
	data = data.sample(frac=1).reset_index(drop=True)
	return(data)

def normalize_data(data, normalize_columns=None):
	if normalize_columns == None:
		data=(data-data.mean())/data.std()
	else:
		for col in normalize_columns:
			data[col] = (data[col]-data[col].mean())/data[col].std()
	return(data)

def divide_data_into_training_test_txt(data, testing_size=.2):
	testing_len = int(len(data)*testing_size)
	test_set = data.iloc[:testing_len]
	training_set = data.iloc[testing_len:]

	np.savetxt("Divine_training_set.txt", training_set, delimiter="\t", fmt="%g")
	np.savetxt("Divine_test_set.txt", test_set, delimiter="\t", fmt="%g")

	# return test_set, training_set
def load_data_from_txt(fileName, colNames):
	return(pd.read_csv(fileName, delimiter='\t', 
		names=colNames))

# Given an x1, x2, and a list of weights, returns a predicted y value
def hypothesis_func(x1, x2, weights):
	return (weights[0] 
		 + (weights[1] * x1)
		 + (weights[2] * x2)
		 + (weights[3] * x1 * x2) 
		 + (weights[4] * (x1 ** 2))
		 + (weights[5] * (x2 ** 2)))

def get_costJ_given_weights(data, weights):
	m = len(data)
	cost_func_sum = 0

	for i, row in data.iterrows():
		x1 = row[0]
		x2 = row[1]
		y = row[2]

		cost_func_sum = cost_func_sum + ((hypothesis_func(x1, x2, weights) - y)**2)

	# print("cost: ", ((1/(2*m)) * cost_func_sum))
	return ((1/(2*m)) * cost_func_sum)

# To aid in updating weights
# Given the index of w (whether its w0, w1, w2, etc), returns the proper x val
def get_x_n(w, x1, x2):
	if w == 0:
		return 1
	elif w == 1:
		return x1
	elif w == 2:
		return x2
	elif w == 3:
		return x1 * x2
	elif w == 4:
		return x1**2
	elif w == 5:
		return x2**2

def user_testing(data, x_cols, weights):
	data_means, data_stds = get_cols_mean_and_std(data, x_cols)

	print("Please test out this regression machine learning algorithm.")
	print("You will be prompted to enter a student's minutes studying per week, \n\tand how many ounces of beer they drink a week.")
	print("To exit the program, enter 0 for each value.")

	get_user_predicted_y_with_x_inputs(weights, data_means, data_stds)

def main():
	gpaData = pd.read_csv("GPAdata.txt",skiprows=[0], delimiter='\t', 
		names=['timPerWeek', 
				'beerPerWeek',  
				'gpa'])
	alpha = .2
	ws = [1] * 6
	iterations = 70
	col_names = ["timPerWeek", "beerPerWeek", "gpa"]
	user_testing()

main()
