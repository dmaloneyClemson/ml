import numpy as np


def g(x):
    return 1.0/(1 + np.exp(-x))

def deriv(x):
    return x*(1.0 - x)

class NeuralNetwork:
    def __init__(self, x, y):
        self.input = x
        self.weight1 = np.random.rand(self.input.shape[1],3)
        self.weight2 = np.random.rand(3,1)
        self.y = y 
        self.output = np.zeros(self.y.shape)
    
    def feedforward(self):
        self.layer1 = g(np.dot(self.input, self.weight1))
        self.output =  g(np.dot(self.layer1, self.weight2))

    def backprop(self):
        d_weight2 = np.dot(self.layer1.T, (2*(self.y-self.output)*deriv(self.output)))
        d_weight1 = np.dot(self.input.T, (np.dot(2*(self.y-self.output)*deriv(self.output), self.weight2.T)*deriv(self.layer1)))
        self.weight1+=d_weight1
        self.weight2+=d_weight2

#main

x = np.array([[1,1],
            [1,0],
            [0,1],
            [0,0]])

y = np.array([[1],[0],[0],[1]])

#neural network
nn = NeuralNetwork(x,y)
for i in range(1500):
    nn.feedforward()
    nn.backprop()

print("the output layer is : ", nn.output)
print()
print("weights 1 = ", nn.weight1)
print("weights 2 = ", nn.weight2)
