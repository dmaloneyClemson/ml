# -*- coding: utf-8 -*-
"""
Created on Fri Sep 20 16:47:58 2019

@author: divin
"""
import statistics 
import matplotlib.pyplot as plt
import pandas as pd
plt.rcParams['figure.figsize'] = (10.0, 8.0)
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


def plotRaw():

       rawData = pd.read_csv("GPAdata.txt",skiprows=[0], delimiter='\t', 
              names=['timPerWeek', 
                     'beerPerWeek',  
                     'gpa'])

       timeWeek = rawData['timPerWeek'].values
       beerWeek = rawData['beerPerWeek'].values
       gpaScores = rawData['gpa'].values

       fig = plt.figure()
       gpa = fig.add_subplot()  

#plt.plot(x, y, color='#58b970', label='Regression Line')

       gpaTime = gpa.scatter(timeWeek,
              gpaScores, 
              color="orange")
       gpaBeer = gpa.scatter(beerWeek,
              gpaScores,
              color="purple")

       gpa.set(title="TimeSpent Studying vs Beer Drinking Per Week", 
       xlabel="TimeSpent Studying", 
       ylabel="Beer Drinking Per Week")
       plt.show()

       
plotRaw()