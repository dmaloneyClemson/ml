import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from statistics import mean, median

global make_plots
global print_values
make_plots = False
print_values = False

# Check if data file exists
def check_file_exists(file_name, col_names):
	try:
		# Try opening the file to check if file valid, close file as to not leaving hanging data
		# os.path.isfile is a faster/more efficient check, but may lead to other issues, does not return r/w permissions
		# open() is a decent check here for a singular data file
		file = open(file_name, "r")
		file.close()
	except FileNotFoundError:
		file_name = input(file_name + " file not found. \nPlease try another file name (try adding the path): ")
		import_data_from_csv(file_name, col_names)
	except:
		print("Unknown error when retreiving data file.")

# Imports the data into a pandas dataframe
def import_data_from_csv(file_name, col_names):
	check_file_exists(file_name, col_names)

	panda_data = pd.read_csv(file_name, skiprows=[0], delimiter="\t",
							names=col_names)

	return (panda_data)

# Plot the data
def plot_data_by_gpa(data):
	rawData = pd.read_csv("GPAdata.txt",skiprows=[0], delimiter='\t', 
		names=['timPerWeek', 
				'beerPerWeek',  
				'gpa'])

	timeWeek = rawData['timPerWeek'].values
	beerWeek = rawData['beerPerWeek'].values
	gpaScores = rawData['gpa'].values

	fig = plt.figure()
	gpa = fig.add_subplot()  

	gpaTime = gpa.scatter(timeWeek,
				gpaScores, 
				color="orange")
	gpaBeer = gpa.scatter(beerWeek,
				gpaScores,
				color="purple")

	gpa.set(title="TimeSpent Studying vs Beer Drinking Per Week", 
	xlabel="TimeSpent Studying", 
	ylabel="Beer Drinking Per Week")
	plt.show()


# Randomizes and returns the imported data
def randomize_data(data):
	# Drops record of previous data positions
	data = data.sample(frac=1).reset_index(drop=True)
	return(data)

# Normalize the data using standard deviation normalization
# If no columns specified, normalizes all data
def normalize_data(data, normalize_columns=None):
	if normalize_columns == None:
		data=(data-data.mean())/data.std()
	else:
		for col in normalize_columns:
			data[col] = (data[col]-data[col].mean())/data[col].std()
	return(data)

# Divides provided data into provided ratio for testing and training data
def divide_data_into_training_test_txt(data, testing_size=.2):
	testing_len = int(len(data)*testing_size)
	# training_len = len(data)*.8
	test_set = data.iloc[:testing_len]
	training_set = data.iloc[testing_len:]

	np.savetxt("trainingSet.txt", training_set, delimiter="\t", fmt="%g")
	np.savetxt("testSet.txt", test_set, delimiter="\t", fmt="%g")
	
	# return test_set, training_set

def load_data_from_txt(fileName, colNames):
	return(pd.read_csv(fileName, delimiter='\t', 
		names=colNames))

# Given an x1, x2, and a list of weights, returns a predicted y value
def hypothesis_func(x1, x2, weights):
	return (weights[0] 
		 + (weights[1] * x1)
		 + (weights[2] * x2)
		 + (weights[3] * x1 * x2) 
		 + (weights[4] * (x1 ** 2))
		 + (weights[5] * (x2 ** 2)))


# Given the dataset and a list of weights, calculates the error cost J
# Does this by summing squared errors for each dataset, then dividing by two & len of the dataset
def get_costJ_given_weights(data, weights):
	m = len(data)
	cost_func_sum = 0
	for i, row in data.iterrows():
		x1 = row[0]
		x2 = row[1]
		y = row[2]

		cost_func_sum = cost_func_sum + ((hypothesis_func(x1, x2, weights) - y)**2)

	# print("cost: ", ((1/(2*m)) * cost_func_sum))
	return ((1/(2*m)) * cost_func_sum)

# To aid in updating weights
# Given the index of w (whether its w0, w1, w2, etc), returns the proper x val
def get_x_n(w, x1, x2):
	if w == 0:
		return 1
	elif w == 1:
		return x1
	elif w == 2:
		return x2
	elif w == 3:
		return x1 * x2
	elif w == 4:
		return x1**2
	elif w == 5:
		return x2**2

# Given the data, a list of previously existing weights, and an alpha value
# Updates the w values using gradient decent
def update_weights(data, weights, alpha):
	# Empty list to store the updated weights in
	new_weights = [None] * len(weights) 
	for w in range(len(weights)):
		m = len(data)
		
		w_old = weights[w]
		# Sum in which to store errors in hypothesis func
		w_temp = 0
		for i, row in data.iterrows():
			x1 = row[0]
			x2 = row[1]
			y = row[2]

			# Get the x value to multiply at the end when calculating new w
			x_n = get_x_n(w, x1, x2)
			w_temp = w_temp + (hypothesis_func(x1, x2, weights) - y) * x_n

		w_new = w_old - alpha * (1/m) * w_temp

		new_weights[w] = w_new

	return new_weights

# Plot the costs over iterations
# Takes user input
def plot_cost_J(iteration, costs):
	global make_plots
	if make_plots == False:
		return

	plotJ = input("Plot J values? y/n ") 
	plot_low = 0
	plot_high = len(iteration)
	if plotJ == "y" or plotJ == "Y":
		print("\nPlot a certain range of iterations? Max iteration is:", len(iteration))
		print("If not, hit enter.")
		print("If so, input range formatted as: lower_val, higher_val. ")
		plotRange = input("J Iterations Range: ")
		try:
			plotRangeL = plotRange
			plotRangeL.split(", ")
			new_plot_low = int(plotRangeL[0])
			new_plot_high = int(plotRangeL[1])
			if new_plot_low < plot_low or new_plot_high > plot_high:
				print("Please enter a valid response: lower_val, higher_val")
				plot_cost_J(iteration, costs)
			else:
				plot_low = new_plot_low
				plot_high = new_plot_high
		except:
			if plotRange == "":
				pass
			else:
				print("\nPlease enter a valid response: lower_val, higher_val")
				plot_cost_J(iteration, costs)
				pass
	
		plt.scatter(iteration[plot_low:plot_high], costs[plot_low:plot_high], color="orange")

		# Plot meta data
		plt.xlabel("Iteration")
		plt.ylabel("Cost (J)")
		plt.title("Cost (J) Over Iterations")
		plt.savefig("maloneyDivine.png")
		plt.show()
		return
	elif plotJ == "n" or plotJ == "N":
		return
	else:
		print("Please enter a valid response (y or n).")
		plot_cost_J(iteration, costs)


# Given a maximum number of iterations to run, the dataset, initial list of weights, and alpha
# Calculates cost of updated weights, and updates weights until max number of iterations
def determine_best_weights_lin_regression_iterations(iterations, data, weights, alpha):
	costs = []
	iteration = []
	for i in range(iterations):
		weights = update_weights(data, weights, alpha)
		cost = get_costJ_given_weights(data, weights)
		costs.append(cost)
		iteration.append(i)

	plot_cost_J(iteration, costs)

	return weights, costs[-1]


def plot_hypo_func(data, weights):
 	max_x1 = data["studying_min"].max()
 	min_x1 = data["studying_min"].min()
 	m = len(data)

 	hypo_val = []
 	x_axis = [min_x1 + x*(max_x1-min_x1)/m for x in range(m)]
 	for i, row in data.iterrows():
 		x1 = row[0]
 		x2 = row[1]
 		y = hypothesis_func(x1, x2, weights)

 		hypo_val.append(y)

 		print("Hypothesized gpa: ", y)
 		print("Actual gpa: ", row[2])
 		print("\n")

 	plt.scatter(data["studying_min"], data["beer_oz"], c=data["gpa"])
 	plt.scatter(data["studying_min"], data["beer_oz"], c=hypo_val)

 	plt.show()

# Returns a list of tuples of predicted ys (using hypothesis func and given weights) and actual ys
def get_predictions_and_actual(data, weights):
	values = []
	for i, row in data.iterrows():
		x1 = row[0]
		x2 = row[1]
		y = hypothesis_func(x1, x2, weights)

		values.append((y, row[2]))

	return values

# Gets the mean and median error between predicted values and actual values
# Takes a list of tuples: ()
def get_mean_and_median_error(predict_and_actual):
	errors = []
	for values in predict_and_actual:
		predict = values[0]
		actual = values[1]
		error = abs(predict - actual)/actual
		errors.append(error)
		# print("Hypothesized gpa: ", predict)
		# print("Actual gpa: ", actual)
		# print ("Error: ", error)
		# print("\n")
		
	# return sum(errors)/len(errors), sorted(errors)[len(errors)/2]
	return mean(errors), median(errors)
	

def get_cols_mean_and_std(data, col_names):
	# cols = {}
	means = {}
	stds = {}
	for col in col_names:
		# cols[col] = [data[col].mean(), data[col].std()]
		means[col] = data[col].mean()
		stds[col] = data[col].std()

	return means, stds

def normalize_x(x, data_mean, data_std):
	xN = (x-data_mean)/data_std
	return xN

def get_user_predicted_y_with_x_inputs(weights, data_means, data_stds):
	print("\n")
	print("Enter the values.")
	try:
		x_study = int(input("Studying Min per Week: "))
		x_beer = int(input("Beer Oz per Week: "))
	except ValueError:
		print("Please enter a valid number.")
		get_user_predicted_y_with_x_inputs(data_means, data_stds)
	except:
		print("Unknown error. Please try again.")
		get_user_predicted_y_with_x_inputs(data_means, data_stds)

	if x_study == 0 and x_beer == 0:
		return

	x_study = normalize_x(x_study, data_means["studying_min"], data_stds["studying_min"])
	x_beer = normalize_x(x_beer, data_means["beer_oz"], data_stds["beer_oz"])
	# print("study and beer normalize: ", x_study, x_beer)
	y = hypothesis_func(x_study, x_beer, weights)
	print("The student's predicted GPA is ", y)
	get_user_predicted_y_with_x_inputs(weights, data_means, data_stds)

def user_testing(data, x_cols, weights):
	data_means, data_stds = get_cols_mean_and_std(data, x_cols)

	print("Please test out this regression machine learning algorithm.")
	print("You will be prompted to enter a student's minutes studying per week, \n\tand how many ounces of beer they drink a week.")
	print("To exit the program, enter 0 for each value.")

	get_user_predicted_y_with_x_inputs(weights, data_means, data_stds)

def main():
	file_name = "GPAData.txt"
	alpha = .2
	weights = [1] * 6
	iterations = 80
	col_names = ["studying_min", "beer_oz", "gpa"]
	studying_min_str = col_names[0]		#x1
	beer_oz_str = col_names[1]			#x2
	gpa_str = col_names[2]				#y				


	# Import data from CSV
	# print("Importing data from original file: ", file_name)
	gpa_data = import_data_from_csv(file_name, col_names)

	mode = 1
	print("\nimplementing the algorithm, please wait.\n")

	# Randomize data order
	gpa_dataR = randomize_data(gpa_data)
	# Normalize body_len and fin_len columns by standard deviation
	gpa_dataN = normalize_data(gpa_dataR, [studying_min_str, beer_oz_str])

	# Divide data into training and testing sets, save them as txts
	divide_data_into_training_test_txt(gpa_dataN, .3)
	
	# Load up training and test sets from txts
	training_set = load_data_from_txt("trainingSet.txt", col_names)
	test_set = load_data_from_txt("testSet.txt", col_names)

	weights, final_J = determine_best_weights_lin_regression_iterations(iterations, training_set, weights, alpha)
	# Pretty flat after 70 iterations, with alpha = .2
	# 100 iterations: [1.1646726514267454, 1.171613373403212, -3.829534049224981e-05, 0.000255983588103698, 0.29557939490555135, 0.002592653225823915]
	# 70 iterations: [1.1361744478942362, 1.1718116595777086, 0.0012120084826641112, 0.0017201704378756052, 0.3072588394103223, 0.013254446196036326]

	if mode == 1:
		user_testing(gpa_data, ["studying_min", "beer_oz"], weights)


main()