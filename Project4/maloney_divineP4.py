# Project 4 Assignment
# Divine Maloney

# Project 4: Spam Filtering using Naïve Bayes Classifier

import math

def removeGarbageText(line):
    
	line = line.lower()
	line = line.strip()
	for letter in line:
		if letter in """[]!.,"-!-A;'`’:#$%^&*()+/?""":
			line = line.replace(letter, " ")
	return(line)

def removeSpaces(line):
	line = line.split(" ")
	line = set(line)
	line.discard("")
	line.discard(" ")
	return(line)


#imports the data and classify if spam or ham
def importData(fileName):

	data = []
	hamTotal = 0
	spamTotal = 0

	with open(fileName, "r", encoding = 'unicode-escape') as f:
		for line in f:
			try:
				spamInt = int(line[0])
			except ValueError:
				print("Incorrect format in data. Email subject not correctly classified as spam (1) or ham (0).")
			except:
				print("Unknown error when importing data: ", fileName)

			if spamInt == 0:
				hamTotal = hamTotal + 1
			elif spamInt == 1:
				spamTotal = spamTotal + 1
			
			subjectLine = line[2:]
			subjectLine = removeGarbageText(subjectLine)
			subjectLine = removeSpaces(subjectLine)

			data.append((spamInt, subjectLine))
	return(data, [hamTotal, spamTotal])


#Bayes equation
def bayesEquation(hamProb, spamProb, hamTotal, spamTotal):
	total = hamTotal + spamTotal

	a = spamProb * spamTotal/total
	b = hamProb * hamTotal/total

	return(1/(1+math.exp(math.log(b)-math.log(a))))

#gets the spam prediction based on input data from file
def getSpamPrediction(hamProb, spamProb, hamTotal, spamTotal):
	bayes = bayesEquation(hamProb, spamProb, hamTotal, spamTotal)

	if bayes >= 0.5:
		return 1
	elif bayes < 0.5:
		return 0

 #gets the frequency of the stopWords 
def getFrequency(data, stopWords):
	dictFrequency = {}
	for line in data:
		spamStatus = line[0]
		subject = line[1]

		for word in subject:
			if word in dictFrequency:
				frequencyList = dictFrequency[word]
				frequencyList[spamStatus] = frequencyList[spamStatus] + 1
				dictFrequency[word] = frequencyList
			else:
				frequencyList = [0, 0]
				frequencyList[spamStatus] = 1
				dictFrequency[word] = frequencyList

	with open(stopWords, "r", encoding = 'unicode-escape') as f:
		for line in f:
			line = line.strip().lower()
			if line in dictFrequency:
				del dictFrequency[line]

	return dictFrequency

#gets the probability, based on the total number of values in the ham/spam
def getProbability(fileName, stopWords):
	data, totals = importData(fileName)
	dictFrequency = getFrequency(data, stopWords)

	hamTotal = totals[0]
	spamTotal = totals[1]

	k = 1

	probabilityDict = {}
	for key,value in dictFrequency.items():
		hasHam = value[0]
		hasSpam = value[1]

		hamProbability = (k + hasHam)/(2*k + hamTotal)
		spamProbability = (k + hasSpam)/(2*k + spamTotal)

		probabilityDict[key] = (hamProbability, spamProbability)

	return probabilityDict

#test to the algorithm vs the data(ham or spam) in the file
def testAlgorithm(fileName, probabilityDict):
	data, totals = importData(fileName)

	hamTotal = totals[0]
	spamTotal = totals[1]

	print("Total number of Spam emails in test set: \t", spamTotal)
	print("Total number of Ham emails in test set: \t", hamTotal)

	truePos = 0
	trueNeg = 0
	falsePos = 0
	falseNeg = 0

	positive = 0
	negative = 1

    #checks the line to see if if its ham or spam and places in ham/spam set
	for line in data:
		spamStatus = line[0]
		subjectLine = line[1]

		linehamProb = 1
		linespamProb = 1
        # looks for the value in created dict them creates a probability based
		for key, value in probabilityDict.items():
			word = key
			wordhamProb = value[0]
			wordspamProb = value[1]

			if word in subjectLine:
				linehamProb = linehamProb * wordhamProb
				linespamProb = linespamProb * wordspamProb
			else:
				linehamProb = linehamProb * (1 - wordhamProb)
				linespamProb = linespamProb * (1 - wordspamProb)

		predictedStatus = getSpamPrediction(linehamProb, linespamProb, hamTotal, spamTotal)
	
        #calculate the truePositive true Negatives
		if spamStatus == positive and predictedStatus == positive:
			truePos = truePos + 1
		elif spamStatus == negative and predictedStatus == negative:
			trueNeg = trueNeg + 1
		elif spamStatus == negative and predictedStatus == positive:
			falsePos = falsePos + 1
		elif spamStatus == positive and predictedStatus == negative:
			falseNeg = falseNeg + 1

	# Calculate accuracy, precision, recall, f1
	accuracy = (truePos + trueNeg)/(truePos + trueNeg + falsePos + falseNeg)
	precision = truePos / (truePos + falsePos)
	recall = truePos / (truePos + falseNeg)
	f1 = 2 * (1/((1/precision) + (1/recall)))

	# print measures
	print("True Positives: \t", truePos)
	print("True Negatives: \t", trueNeg)
	print("False Positives: \t", falsePos)
	print("False Negatives: \t", falseNeg)
	print("Accuracy: \t", round(accuracy,2))
	print("Precision: \t", round(precision,2))
	print("Recall: \t", round(recall,2))
	print("F1: \t\t", round(f1,2))

def main():
	trainingSetFile = "GEASTrain.txt"
	testSetFile = "GEASTest.txt"
	stopWordsFile = "StopWords.txt"

	print("\nProvide training data file name.")
	userFile = str(input("File name: "))

	print("\nProvide stop words file name.")
	userFile = str(input("File name: "))


	dictWordPrbobility = getProbability(trainingSetFile, stopWordsFile)

	print("\nProvide test data file name.")
	userFile = str(input("File name: "))

	testAlgorithm(testSetFile, dictWordPrbobility)


main()





