#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  4 09:14:27 2019

@author: Dane Acena

Project 4: Spam Filtering using Naïve Bayes Classifier

Problem Description: Build a Naïve Bayes Classifier algorithm to filter spam. 
The provided training set and test set are provided through canvas. Both files 
have the same format. Each line will start with either a 1 (spam, positive
condition) or 0 (ham), then a space followed by the email subject line. A third
file is also provided which contains a list of stop words. These stop words are
common words that shoulbe be removed from the vocabulary list. The format for 
the stop word list is one word per line.
    
Assignment: Prompt the user for the name of the training set file in the format
described above and the name of the file of Stop Words. The program should 
create a vocabulary of words found in the subject lines of the training set 
associated with an estimated probability of each word appearing in a Spam and 
the estimated probability of each word appearing in a Ham email. Your program 
should then prompt the user for a labeled test set and predict the cass (1 = 
spam, 0 = ham) of each subject line using a Naïve Bayes approach as discussed 
in class.

References used:
    https://hackernoon.com/how-to-build-a-simple-spam-detecting-machine-learning-classifier-4471fe6b816e
    https://github.com/agarwalgaurav811/Spam-classifier
    
"""
# stop words: i a about an are as at be by com

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import itertools


def loadEmailSet(filename, stopWords):
    total_spam = total_ham = 0
    counted = dict()
    file = open(filename, "r", encoding = 'unicode-escape')
    textLine = file.readline()
    while textLine != "":
        
        is_spam = int(textLine[:1])
        if is_spam == 1:
            total_spam += 1
        else:
            total_ham += 1
        

        textLine = tokenize(textLine[1:])        
        words = textLine.split()
        words = set(words)
        filteredSentence = filterSentence(words, stopWords)
        counted = countWords(filteredSentence, is_spam, counted)
        textLine = file.readline()
        
    wordProb = (wordProbabilities(1, counted,total_spam,total_ham))
    total_email = total_spam + total_ham
    pA = total_spam/total_email
    print(pA)
    pNotA = total_ham/total_email
    print(pNotA)
    print(pA + pNotA)
    file.close

def spamProbabilities(message):
    messageWords = tokenize(message)
    spamProb = hamProb = 0
    print(messageWords)
    
def wordProbabilities(k, count, total_spam, total_ham):
    for each_key in count:
        count[each_key][0] = (count[each_key][0] + k)/(2*k+total_ham)
        count[each_key][1] = (count[each_key][1] + k)/(2*k+total_spam)
    return count
        
def tokenize(text):
    text = text.lower()
    text = text.strip()
    for letters in text:
        if letters in """[]!.,"-!-A;':#$%^&*()+/?""":
            text = text.replace(letters, " ")
    return text

def filterSentence(words, stopWords):
    excl = open(stopWords, "r", encoding = 'unicode-escape')
    excl = excl.read()
    excl = excl.split()
    
    filteredSentence = [w for w in words if not w in excl]
    filteredSentence = []
    
    for each_word in words:
        if each_word not in excl:
            filteredSentence.append(each_word)
    
    return filteredSentence

def countWords(words, is_spam, counted):  
    print(words)    
    for each_word in words:
        if each_word in counted:
            if is_spam == 1:
                counted[each_word][1] = counted[each_word][1] + 1
            else: 
                counted[each_word][0] = counted[each_word][0] + 1
        else:
            if is_spam == 1:
                counted[each_word] = [0,1]
            else:
                counted[each_word] = [1,0]
    return counted

def main():
    print("""
         +-+-+-+-+-+ +-+-+-+-+-+ +-+-+-+-+-+-+-+-+-+-+
         |N|a|ï|v|e| |B|a|y|e|s| |C|l|a|s|s|i|f|i|e|r|
         +-+-+-+-+-+ +-+-+-+-+-+ +-+-+-+-+-+-+-+-+-+-+                    
         """)
    
    #Preprocessing, importing data    
    subjectTrainFile = "GEASTrain.txt" #TODO: Change to user input
    stopWordsFile  = "StopWords.txt" #TODO: Change to user input
    
    #loadEmailSet(subjectTrainFile, stopWordsFile)
    
    subjectTestFile = "GEASTest.txt" #TODO: Change to user input
main()

##gives the conditional probability p(B_i | A_x)
#def conditionalWord(word, spam):
#    if spam:
#       return trainPositive[word]/(float)positiveTotal
#    return trainNegative[word]/(float)negativeTotal
#
##gives the conditional probability p(B | A_x)
#def conditionalEmail(body, spam):
#    result = 1.0
#    for word in body:
#        result *= conditionalWord(word, spam)
#    return conditionalEmail
#
##classifies a new email as spam or not spam
#def classify(email):
#    isSpam = pA * conditionalEmail(email, True) # P (A | B)
#    notSpam = pNotA * conditionalEmail(email, False) # P(¬A | B)
#    return isSpam > notSpam

###############################################################################
###############################################################################
###############################################################################
#def confusionMatrix(y,outcome):
#    f1_score = k = tp = fp = fn = tn = 0
#    for i in range(len(outcome)):
#        if outcome[i] == y[i]:
#            k = k + 1
#        if outcome[i] == y[i] == 0:
#            tp = tp + 1
#        if outcome[i] == 0 and y[i] == 1:
#            fp = fp + 1
#        if outcome[i] == 1 and y[i] == 0:
#            fn = fn + 1
#        if (outcome[i] == y[i] == 1):
#            tn = tn + 1
#            
#    accuracy = k/len(y)
#    precision = tp/(tp + fp)
#    recall = tp/(tp + fn)
#    f1_score = (2 * precision * recall)/(precision + recall)
#            
##    cm = np.array([[tp, fn], [fp, tn]])
#    
#    print("True Positive: ", tp)
#    print("True Negative: ", tn)
#    print("False Positive: ", fp)
#    print("False Negative: ", fn)
#    print("Recall: ", recall)
#    print("precision: ", precision)
#    print("Accuracy: ", accuracy)
#    print("F1 Score: ", f1_score)
#    
#        # Un-Normalized Confusion Matrix...
##    plotConfusionMatrix(cm, classes=[0,1])
#        
#def plotConfusionMatrix(cm, classes):
#    """
#    This function prints and plots the confusion matrix.
#    Normalization can be applied by setting `normalize=True`.
#    """
#    cmap = plt.cm.Blues
#    print('Confusion matrix, without normalization')
##    print(cm)
#    plt.imshow(cm, interpolation='nearest', cmap=cmap)
#    plt.title("Confusion Matrix")
#    plt.colorbar()
#    tick_marks = np.arange(len(classes))
#    plt.xticks(tick_marks, classes, rotation=45)
#    plt.yticks(tick_marks, classes)
#    thresh = cm.max() / 2.
#        
#    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
#        plt.text(j, i, format(cm[i, j]),horizontalalignment="center",color="white" if cm[i, j] > thresh else "black")
#    
#    plt.tight_layout()
#    plt.ylabel('True label')
#    plt.xlabel('Predicted label')
#    plt.show()
#
#
#    
#def userInputs(data,theta):
#    twoCols = data[['Body Length','Dorsal Fin Length']]
#    dataColMean = {}
#    dataColStd = {}
#    
#    for col in twoCols:
#        dataColMean[col] = twoCols[col].mean()
#        dataColStd[col] = twoCols[col].std()
#    
#    while True:
#        bodyLength = float(input("Input Body Length: "))
#        dorsalLength = float(input("Input Dorsal Fin Length: "))
#        if bodyLength == 0 and dorsalLength == 0:
#            break
#        elif bodyLength < 0 or dorsalLength < 0:
#            print("Lengths needs to be positive values.")
#        elif bodyLength > 0 or dorsalLength > 0:
#            bodyLengthMean = dataColMean['Body Length']
#            bodyLengthStd = dataColStd['Body Length']
#            dorsalMean = dataColMean['Dorsal Fin Length']
#            dorsalStd = dataColStd['Dorsal Fin Length']
#            
#            bodyNorm = (bodyLength - bodyLengthMean)/bodyLengthStd
#            dorsalNorm = (dorsalLength - dorsalMean)/dorsalStd
#
#            X = {'Body Length':[bodyNorm], 'Dorsal Fin Length':[dorsalNorm]}
#            X = pd.DataFrame(X)
#           
#            print("That fish is predicted to be: TigerFish" + str(prediction[0]))
#        else:
#            print("Input must be either integer or float")
###############################################################################
###############################################################################
###############################################################################   