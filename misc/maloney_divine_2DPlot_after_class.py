"""
Divine Maloney
September 6, 2019
CPSC 6820 : Intro to Machine Learning
Dr. Hodges

Write a python program that does a single plot of sepal length verses petal length for all three varieties of iris flowers. 
Be sure to label your axes and print a title to your plot.  
Name your program yourlastname_yourfirstname_2DPlot.py. 
Your program should read in and open the file named IrisData.txt 
and write out the plot to a png file named yourlastname_yourfirstname_MyPlot.png 
and to the screen when executed.
"""
import matplotlib.pyplot as plt
import pandas as pd


IrisData = pd.read_csv("IrisData.txt", 
                      delimiter = '\t',
                      names=['sepalLength','sepalWidth','petalLength','petalWidth','flowerType']
                       )

setosa = IrisData.loc[IrisData.flowerType =='setosa']
virginica = IrisData.loc[IrisData.flowerType =='virginica']
versicolor = IrisData.loc[IrisData.flowerType =='versicolor']


plt.scatter(setosa['sepalLength'],
                    setosa['petalLength'], 
                    color="purple")

plt.scatter(virginica['sepalLength'],
                       virginica['petalLength'],
                       color="orange")

plt.scatter(versicolor['sepalLength'],
                        versicolor['petalLength'], 
                        color="blue")


#SAVING PLOT TO PNG
#Output: Write the plot into a PNG file named "maloney_divine_MyPlot.png"
plt.savefig("maloney_divine_MyPlot.png")
plt.show()


